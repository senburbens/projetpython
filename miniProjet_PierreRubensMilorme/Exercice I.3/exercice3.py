# -*-coding:Latin-1 -*

import os
import string

""" Word frequency analysis  """
""" Exercise I.3 """

debut = False
total = 0
listeComplete = []
nombre_occurences={}
fichier = open("fichier.txt","r")

for ligne in fichier.readlines():
    if ligne.find("*** START OF THIS PROJECT GUTENBERG EBOOK") != -1:
        debut = True
    elif ligne.find("*** END OF THIS PROJECT GUTENBERG EBOOK") != -1: 
        debut = False       
    elif debut and ligne.find("*** START OF THIS PROJECT GUTENBERG EBOOK") == -1 and len(ligne) > 1:            
        ligne_nettoyee=ligne.strip()
        liste=ligne_nettoyee.split()        
        #creation de variables temporaires
        nouveauxMots=[]     
        
        for mot in liste :
            mot=mot.translate(string.maketrans("",""), string.punctuation).lower()
            if mot != "" :
                nouveauxMots=nouveauxMots + [mot]
                total = total + 1
                listeComplete = listeComplete + nouveauxMots
                #on compte le nombre d'occurences de chaque mot
                cles=nombre_occurences.keys() #On recupere les cles
                if mot in cles :
                    nombre_occurences[mot]=nombre_occurences[mot]+1
                else  :
                    nombre_occurences[mot]=1                    

inversee=[(valeur,cle) for cle,valeur in nombre_occurences.items()]
inversee.sort(reverse=True)
liste_reinversee=[(cle,valeur) for valeur,cle in inversee]
print "voici les 20 mots les plus frequents\n"
for i in range(20):
    print liste_reinversee[i]
print "\n\n"
    
   
os.system("pause")
