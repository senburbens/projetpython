# -*-coding:Latin-1 -*

import os
import string

""" Word frequency analysis  """
""" Exercise I.4 """

debut = False
total = 0
listeComplete = []
nombre_occurences={}

liste=["the","an","is"]
    
fichier = open("fichier.txt","r")

for ligne in fichier.readlines():
    if ligne.find("*** START OF THIS PROJECT GUTENBERG EBOOK") != -1:
        debut = True
    elif ligne.find("*** END OF THIS PROJECT GUTENBERG EBOOK") != -1: 
        debut = False       
    elif debut and ligne.find("*** START OF THIS PROJECT GUTENBERG EBOOK") == -1 and len(ligne) > 1:            
        ligne_nettoyee=ligne.strip()
        liste=ligne_nettoyee.split()        
        #creation de variables temporaires
        nouveauxMots=[]     
        
        for mot in liste :
            mot=mot.translate(string.maketrans("",""), string.punctuation).lower()
            if mot != "" :
                nouveauxMots=nouveauxMots + [mot]
                total = total + 1
                listeComplete = listeComplete + nouveauxMots
                #on compte le nombre d'occurences de chaque mot
                cles=nombre_occurences.keys() #On recupere les cles
                if mot in cles :
                    nombre_occurences[mot]=nombre_occurences[mot]+1
                else  :
                    nombre_occurences[mot]=1                    


cles=nombre_occurences.keys()
print "Voici les mots du livre qui ne sont pas dans la liste de mots"
for cle in cles :
    if cle not in liste :
        print cle
   
os.system("pause")
