# -*-coding:Latin-1 -*


import random
import os

""" Random Numbers """
""" exercice 2.1"""


#fonction renvoyant l'histogramme avec la probabilite de chaque element
def choose_from_hist(liste):    
        dico={}
        liste1=[]
        #Parcours de la liste pour compter les elements
        for elem in liste :
            if elem not in dico.keys() :
                dico[elem]=1
            else :
                    dico[elem]=dico[elem]+1
                    
        #Calcul de la probabilite de l'element            
        for cle in dico :
            dico[cle]=str(dico[cle])+"/"+str(len(liste))
        for cle in dico:
            liste1.append(cle)
        randomNumber=random.randint(0,len(liste1)-1)
        return liste1[randomNumber],dico[liste1[randomNumber]] #on renvoie le dictionnaire

#test
if __name__ == "__main__":
    liste1=['a','a','b','c','c','d','e']
    print "liste : ",liste1
    
    while True :
        retry=str("y")
        mot,probabilite=choose_from_hist(liste1)
        print "probabilite de ",mot,":",probabilite
        
        retry=input("Recommencer ?")

        retry.lower()
        
        while retry!=str("n") and retry !=str("y") :
            retry=input("Recommencer ?\noui pour recommencer\tnon pour annuler")
            retry.lower()
        if retry=="n" :
            break
            
        os.system("pause")
    
