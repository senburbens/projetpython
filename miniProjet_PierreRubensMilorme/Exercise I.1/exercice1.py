# -*-coding:Latin-1 -*

import os
import string

""" Word frequency analysis  """
""" Exercise I.1. """


contenu=list()
with open("fichier.txt","r") as fichier :
    for ligne in fichier.readlines() :
            liste=ligne.split(' ')
            for word in liste :
                word=word.strip()
                word=word.translate(string.maketrans("",""), string.punctuation).lower()
                if len(word)>0 :
                    contenu.append(word)
print " ".join(contenu)
os.system("pause")
        
